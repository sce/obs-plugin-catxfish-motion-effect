# ubuntu 18.04 LTS aka bionic
FROM ubuntu:bionic

# https://github.com/obsproject/obs-studio/wiki/Install-Instructions#debian-based-build-directions
RUN apt-get update && \
	apt-get install -y \
	build-essential \
	checkinstall \
	cmake \
	git \
	libmbedtls-dev \
	libasound2-dev \
	libavcodec-dev \
	libavdevice-dev \
	libavfilter-dev \
	libavformat-dev \
	libavutil-dev \
	libcurl4-openssl-dev \
	libfdk-aac-dev \
	libfontconfig-dev \
	libfreetype6-dev \
	libgl1-mesa-dev \
	libjack-jackd2-dev \
	libjansson-dev \
	libluajit-5.1-dev \
	libpulse-dev \
	libqt5x11extras5-dev \
	libspeexdsp-dev \
	libswresample-dev \
	libswscale-dev \
	libudev-dev \
	libv4l-dev \
	libvlc-dev \
	libx11-dev \
	libx264-dev \
	libxcb-shm0-dev \
	libxcb-xinerama0-dev \
	libxcomposite-dev \
	libxinerama-dev \
	pkg-config \
	python3-dev \
	qtbase5-dev \
	libqt5svg5-dev \
	swig \
  vim && \
  apt-get clean -y

# The only thing added extra to the above list is vim.

#USER ubuntu
WORKDIR /root

#COPY obs-studio obs-studio
#COPY motion-effect motion-effect

# These two variables decide which version of OBS and the motion-effect plugin
# will be cloned and compiled:
ENV OBS_VER=25.0.3
ENV MEFFECT_VER=1.2.0

COPY src/compile .
COPY src/compile-obs .
COPY src/compile-plugin .

CMD ["./compile"]
