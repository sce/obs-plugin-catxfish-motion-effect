# obs-plugin-catxfish-motion-effect

[This repository][1] contains the Dockerfile needed to compile the [motion-effect][2] plugin for
[obs-studio][3] (Open Broadcaster Software Studio).

[1]: https://gitlab.com/sce/obs-plugin-catxfish-motion-effect
[2]: https://github.com/CatxFish/motion-effect
[3]: https://github.com/obsproject/obs-studio

## Requirements

`docker` needs to be installed (and daemon running) or equivalent utility (e.g. `podman` should
probably work).

## Build

Simply run:

    $ ./build-and-run

This will build the docker image and run it. The docker image will:

1) install all dependencies needed
2) fetch the version of obs-studio and motion-effect plugin as decided in the Dockerfile
3) compile obs-studio and motion-effect plugin
4) put the resulting output into the `out` directory outside the container

## Poking around inside the container

Instead of just compiling the plugin it is possible to explore the container by running the
convenience script:

    $ ./run

This will build the docker image. The docker image will:

1) install all dependencies needed
2) start an interactive bash session inside the container

In order to actually download and compile everything inside the container just issue `./compile`.
